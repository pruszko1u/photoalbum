package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;

public class Page extends AbstractModel {

	private ObservableList<Picture> pictureList = FXCollections.observableArrayList();
	private int pageNumber;
	private WritableImage pageSnapshot;
	
	/**
	 * Constructor Page >>> sets the attribute pageNumber.
	 * @param pageNumber : Integer
	 */
	public Page (int pageNumber) {
		this.pageNumber = pageNumber;
		this.pageSnapshot = null;
	}
	
	/**
	 * Method setPositionImage >>> changes the x and y position of an image (called after a drag and drop)
	 * @param id : String
	 * @param x : Double 
	 * @param y : Double
	 */
	public void setPositionImage (String id, double x, double y) {
		for (Picture picture : this.pictureList) {
			if (id.equals(picture.getId())) {
				picture.setX(x -(picture.getWidth()/2));
				picture.setY(y -(picture.getHeight()/2));
			}
		}
	}
	
	/**
	 * Method setTailleImage >>> sets the height and width attributes from a given image.
	 * @param id : int
	 * @param width : int
	 * @param height : int
	 */
	public void setTailleImage(String id, int width, int height) {
		for (Picture picture : this.pictureList) {
			if (id.equals(picture.getId())) 
			{
				/* 
				 * In order to prevent the picture to get out of the scope when resizing, we modify the x and y 
				 * attributes to keep the same center 
				*/
				double x = (picture.getX() + (picture.getWidth() / 2)) - (width / 2);
				double y = (picture.getY() + (picture.getHeight() / 2) - (height / 2));
				picture.setX(x);
				picture.setY(y);
				picture.setHeight(height);
				picture.setWidth(width);
			}
		}
	}
	
	/**
	 * Constructor Page >>> sets the attribute pictureList
	 * @param pictureList : ObservableList<Picture>
	 */
	public Page (ObservableList<Picture> pictureList) {
		this.pictureList = pictureList;
		this.pageSnapshot = null;
	}
	
	/**
	 * Method addImage >>> add a new image to the picture list.
	 * @param picture : Picture
	 */
	public void addImage(Picture picture) {
		this.pictureList.add(picture);
	}
	
	/**
	 * Method setPictureList >>> sets the attribute pictureList
	 * @param pictureList : ObservableList<Picture>
	 */
	public void setPictureList (ObservableList<Picture> pictureList) {
		this.pictureList = pictureList;
	}
	
	/**
	 * Method getPictureList >>> gets the attribute pictureList
	 * @return pictureList : ObservableList<Picture>
	 */
	public ObservableList<Picture> getPictureList() {
		return this.pictureList;
	}
	
	/**
	 * Method getPageNumber >>> gets the attribute pageNumber
	 * @return pageNumber : int
	 */
	public int getPageNumber() {
		return this.pageNumber;
	}
	
	/**
	 * Method setPageNumber >>> sets the attribute pageNumber
	 * @param pageNumber
	 */
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	
	/**
	 * Method setPageSnapshot >>> sets the page's snapshot attribute 
	 * @param snapshot : WritableImage
	 */
	public void setPageSnapshot(WritableImage snapshot) {
		this.pageSnapshot = snapshot;
	}
	
	/**
	 * Method getPageSnapshot >>> returns the page's snapshot attribute
	 * @return pageSnapshot : WritableImage
	 */
	public WritableImage getPageSnapshot() {
		return this.pageSnapshot;
	}

	/**
	 * Method setDisplayTitleImage >>> gets whether a given picture's title should be displayed.
	 * @param id : String
	 * @param value : boolean
	 */
	public void setDisplayTitleImage(String id, boolean value) 
	{	
		for (Picture picture : this.pictureList) {
			if (id.equals(picture.getId())) 
			{
				picture.setDisplayTitle(value);
			}
		}
	}
	
	/**
	 * Method setCadreImage >>> sets the cadre attribute of a given picture.
	 * @param id : String 
	 * @param cadre : String
	 */
	public void setCadreImage(String id, String cadre) 
	{
		for (Picture picture : this.pictureList) {
			if (id.equals(picture.getId())) 
			{
				picture.setCadre(cadre);
			}
		}
	}
	
	/**
	 * Method setCadreColorImage >>> sets the cadreColor attribute of a given picture.
	 * @param id : String 
	 * @param color : String
	 */
	public void setCadreColorImage(String id, String color) 
	{
		for (Picture picture : this.pictureList) {
			if (id.equals(picture.getId())) 
			{
				picture.setCadreColor(color);
			}
		}
	}

	/**
	 * Method setDisplayCadreImage >>> gets whether a given picture's border should be displayed.
	 * @param id : String
	 * @param value : boolean
	 */
	public void setDisplayCadreImage(String id, boolean value) 
	{
		for (Picture picture : this.pictureList) {
			if (id.equals(picture.getId())) 
			{
				picture.setDisplayCadre(value);
			}
		}
	}
	
	/**
	 * Method setTitreImage >>> sets the title attribute of a given picture.
	 * @param id : String
	 * @param title : String
	 */
	public void setTitreImage(String id, String title) 
	{
		for (Picture picture : this.pictureList) {
			if (id.equals(picture.getId())) 
			{
				picture.setName(title);
			}
		}
	}

	/**
	 * Method getWidthImage >>> gets the width attribute of a given picture.
	 * @param id : String
	 * @return int
	 */
	public int getWidthImage(String id) 
	{
		for (Picture picture : this.pictureList) {
			if (id.equals(picture.getId())) 
			{
				return picture.getWidth();
			}
		}
		return 0;
	}
	
	/**
	 * Method getCadreImage >>> gets the cadre attribute of a given picture.
	 * @param id : String 
	 * @return cadre : String
	 */
	public String getCadreImage(String id) 
	{
		for (Picture picture : this.pictureList) {
			if (id.equals(picture.getId())) 
			{
				return picture.getCadre();
			}
		}
		return "";
	}
	
	/**
	 * Method getCadreColorImage >>> gets the cadreColor attribute of a given picture.
	 * @param id : String 
	 * @return color : String
	 */
	public String getCadreColorImage(String id) {
		for (Picture picture : this.pictureList) {
			if (id.equals(picture.getId())) 
			{
				return picture.getCadreColor();
			}
		}
		return "";
	}

	/**
	 * Method getTitreImage >>> gets a given picture's title.
	 * @param id : String
	 * @return String
	 */
	public String getTitreImage(String id) 
	{
		for (Picture picture : this.pictureList) {
			if (id.equals(picture.getId())) 
			{
				return picture.getName();
			}
		}
		return null;
	}

	/**
	 * Method getHeightImage >>> gets a given picture's height.
	 * @param id : String 
	 * @return int
	 */
	public int getHeightImage(String id) 
	{
		for (Picture picture : this.pictureList) {
			if (id.equals(picture.getId())) 
			{
				return picture.getHeight();
			}
		}
		return 0;
	}
	
	/**
	 * Method isDisplayTitleImage >>> gets whether a given picture's title should be displayed.
	 * @param id : String
	 * @return boolean
	 */
	public boolean isDisplayTitleImage(String id) 
	{
		for (Picture picture : this.pictureList) {
			if (id.equals(picture.getId())) 
			{
				return picture.isDisplayTitle();
			}
		}
		return false;
	}

	/**
	 * Method isDisplayCadreImage >>> gets whether a given picture's border should be displayed.
	 * @param id : String
	 * @return boolean
	 */
	public boolean isDisplayCadreImage(String id) {
		for (Picture picture : this.pictureList) {
			if (id.equals(picture.getId())) 
			{
				return picture.isDisplayCadre();
			}
		}
		return false;
	}



	
	
}
