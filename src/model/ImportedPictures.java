package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ImportedPictures extends AbstractModel {

	private ObservableList<Picture> pictureList = FXCollections.observableArrayList();

	/**
	 * Constructor ImportedPictures
	 */
	public ImportedPictures () {}
	
	/**
	 * Constructor ImportedPictures >>> sets the pictureList attribute.
	 */
	public ImportedPictures (ObservableList<Picture> pictureList) {
		this.pictureList = pictureList;
	}
	
}
