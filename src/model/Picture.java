package model;

import java.util.UUID;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Picture extends AbstractModel {

	private String id;
	private Image image;
	
	private String title;
	private boolean displayTitle = false;
	
	private int imageWidth;
	private int imageHeight;
	
	private double x;
	private double y;
	
	private String cadre;
	private String cadreColor;
	private boolean displayCadre = false;
	
	/**
	 * Consctructor Picture >>> sets the attribute id to a random id
	 */
	public Picture () {
		this.id = UUID.randomUUID().toString();
	}
	
	/**
	 * Constructor Picture >>> sets the attribute image and id to a random id
	 * @param image : Image
	 */
	public Picture (Image image) {
		this.id = UUID.randomUUID().toString();
		this.image = image;
		this.title = "";
		
		this.imageWidth = (int) image.getWidth();
		this.imageHeight = (int) image.getHeight();
		
		this.cadre = "";
		this.cadreColor = "";
		
	}	

	/**
	 * Constructor Picture >>> sets the attributes image, name, width and height
	 * @param image : Image
	 * @param name : String
	 * @param width : int
	 * @param height : int
	 */
	public Picture (Image image, String name, int width, int height) {
		this.id = UUID.randomUUID().toString();
		this.image = image;
		this.title = name;
		
		
		this.imageWidth = width;
		this.imageHeight = height;
	}	
	
	// Getters / Setters

	/**
	 * Method setDisplayTitle >>> sets wether the title should be displayed.
	 * @param value : boolean
	 */
	public void setDisplayTitle(boolean value) {
		this.displayTitle = value;
	}
	
	/**
	 * Method setDisplayCadre >>> sets wether the border should be displayed.
	 * @param value : boolean
	 */
	public void setDisplayCadre(boolean value) 
	{
		this.displayCadre = value;
	}

	/**
	 * Method setName >>> sets the attribute name
	 * @param name
	 */
	public void setName (String name) {
		this.title = name;
	}
	
	/**
	 * Method getName >>> gets the attribute name
	 * @return imageName : String
	 */
	public String getName () {
		return this.title;
	}
	
	/**
	 * Method setWidth >>> sets the attribute width
	 * @param width
	 */
	public void setWidth(int width) {
		this.imageWidth = width;
	}
	
	/**
	 * Method getWidth >>> gets the attribute width
	 * @return width : int
	 */
	public int getWidth () {
		return this.imageWidth;
	}
	
	/**
	 * Method setHeight >>> sets the attribute height
	 * @param height
	 */
	public void setHeight (int height) {
		this.imageHeight = height;
	}
	
	/**
	 * Method getHeight >>> gets the attribute height
	 * @return height : int
	 */
	public int getHeight () {
		return this.imageHeight;
	}

	/**
	 * Method getImage >>> gets the attribute image
	 * @return image : Image
	 */
	public Image getImage() {
		return this.image;
	}
	
	/**
	 * Method setImage >>> sets the attribute image
	 * @param img
	 */
	public void setImage(Image img) {
		this.image = img;
	}
	
	/**
	 * Method getId >>> gets the attribute id
	 * @return id : String
	 */
	public String getId() {
		return this.id;
	}
	
	/**
	 * Method getX >>> gets the attribute x
	 * @return x : double
	 */
	public double getX() {
		return this.x;
	}
	
	/**
	 * Method setX >>> sets the attribute x
	 * @param x
	 */
	public void setX(double x) {
		this.x = x;
	}
	
	/**
	 * Method getY >>> gets the attribute y
	 * @return y : double
	 */
	public double getY() {
		return this.y;
	}
	
	/**
	 * Method setY >>> sets the attribute y
	 * @param y
	 */
	public void setY(double y) {
		this.y = y;
	}
	
	/**
	 * Method getCadre >>> gets the cadre attribute
	 * @return cadre : String
	 */
	public String getCadre() {
		return cadre;
	}

	/**
	 * Method setCadre >>> sets the cadre attribute
	 * @param cadre : String
	 */
	public void setCadre(String cadre) {
		this.cadre = cadre;
	}

	/**
	 * Method getCadreColor >>> gets the cadreColor atttribute
	 * @return cadreColor : String
	 */
	public String getCadreColor() {
		return cadreColor;
	}

	/**
	 * Method setCadreColor >>> sets the cadreColor attribute
	 * @param color : String
	 */
	public void setCadreColor(String color) {
		this.cadreColor = color;
	}

	/**
	 * Method getTitle >>> gets the title attribute
	 * @return title : String
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Method getTitle >>> sets the title attribute
	 * @param title : String
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Method isDisplayTitle >>> gets the displayTitle attribute.
	 * @return displayTitle : boolean
	 */
	public boolean isDisplayTitle() {
		return displayTitle;
	}

	/**
	 * Method isDisplayCadre >>> gets the displayCadre attribute.
	 * @return displayCadre : boolean
	 */
	public boolean isDisplayCadre() {
		return displayCadre;
	}
	
}
