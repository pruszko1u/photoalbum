package model;

public class Home extends AbstractModel {

	private Album albumModel;
	private ImportedPictures importedPicturesModel;
	
	/**
	 * Constructor Home
	 */
	public Home () {}
	
	/**
	 * Constructor Home >>> sets the albumModel and importedPicturesModel attributes.
	 * @param albumModel : Album
	 * @param importedPicturesModel : ImportedPictures
	 */
	public Home (Album albumModel, ImportedPictures importedPicturesModel) {
		this.albumModel = albumModel;
		this.importedPicturesModel = importedPicturesModel;
	}
	
}
