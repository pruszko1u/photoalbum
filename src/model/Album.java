package model;

import java.io.File;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;

public class Album extends AbstractModel {

	private ObservableList<Page> pageList = FXCollections.observableArrayList();
	private ObservableList<Picture> images = FXCollections.observableArrayList();
	
	/**
	 * Constructor Album
	 */
	public Album () {}
	
	/**
	 * Constructor Album >>> sets the pageList parameter
	 * @param pageList : ObservableList<Page>
	 */
	public Album(ObservableList<Page> pageList) {
		this.pageList = pageList;
	}
	
	/**
	 * Method addImage >>> adds a picture to the attribute images.
	 * @param file : File
	 */
	public void addImage(File file) 
	{
		Image i = new Image(file.toURI().toString());
		this.images.add(new Picture(i));
		
	}
	
	/**
	 * Method addPage >>> adds a page to the attribute pageList
	 * @return newPageNumber : int
	 */
	public int addPage() {
		int newPageNumber = pageList.size();
		Page page = new Page(newPageNumber);
		pageList.add(page);
		
		return newPageNumber;
	}
	
	/**
	 * Method pageExist >>> checks wether a given pageNumber matches a page or not.
	 * @param pageNumber : int
	 * @return res : boolean
	 */
	public boolean pageExist(int pageNumber) {
		
		boolean res = false;
		
		for (Page page : pageList) {
			if (page.getPageNumber() == pageNumber) {
				res = true;
			}
		}
		
		return res;
	}
	
	/**
	 * Method getPicturesFromPage >>> gets every pictures from a given page.
	 * @param pageNumber : int
	 * @return picturesList : ObservableList<Picture>
	 */
	public ObservableList<Picture> getPicturesFromPage(int pageNumber) {
		
		ObservableList<Picture> picturesList = null;
		
		for (Page page : pageList) {
			if (page.getPageNumber() == pageNumber) {
				picturesList = page.getPictureList();
			}
		}
		
		return picturesList;
	}
	
	/**
	 * updatePageSnapshot >>> updates the snapshot from a given page.
	 * @param snapshot : WritableImage
	 * @param pageNumber : int
	 */
	public void updatePageSnapshot(WritableImage snapshot, int pageNumber) {
		for (Page page : pageList) {
			if (page.getPageNumber() == pageNumber) {
				page.setPageSnapshot(snapshot);
			}
		}
	}
	
	/**
	 * Method addImageAlbum >>> adds a new picture to a given page, using both
	 * x and y properties.
	 * @param pageNumber : int
	 * @param id : String
	 * @param x : double
	 * @param y : double
	 */
	public void addImageAlbum (int pageNumber, String id, double x, double y) {
		
		Picture picture = null;
		
		for (Picture pic : images) {
			if (id.contains(pic.getId())) 
			{
				picture = new Picture(pic.getImage());
				picture.setX(x-(picture.getWidth()/2));
				picture.setY(y-(picture.getHeight()/2));
			}
		}
		
		if (picture != null) {
			for (Page page : pageList) {
				if (page.getPageNumber() == pageNumber) {
					page.addImage(picture);
				}
			}
		}
		
	}
	
	/**
	 * Method setPositionImagePage >>> change the position of the picture inside a given page
	 * according to both x and y properties.
	 * @param pageNumber : int
	 * @param id : String 
	 * @param x : double
	 * @param y : double
	 */
	public void setPositionImagePage (int pageNumber, String id, double x, double y) {
		for (Page page : pageList) {
			if (page.getPageNumber() == pageNumber) {
				page.setPositionImage(id, x, y);
			}
		}
	}
	
	/**
	 * Method setTailleImagePage >>> sets the width and height of a picture inside a given page.
	 * @param pageNumber : int
	 * @param id : String
	 * @param width : int
	 * @param height : int
	 */
	public void setTailleImagePage(int pageNumber, String id, int width, int height) 
	{
		for (Page page : pageList)
		{
			if (page.getPageNumber() == pageNumber) {
				page.setTailleImage(id, width, height);
			}
		}
	}
	
	/**
	 * Method setDisplayTitleImagePage >>> displays or hides the title of a given picture from a 
	 * given page.
	 * @param pageNumber : int
	 * @param id : String
	 * @param value : boolean
	 */
	public void setDisplayTitleImagePage(int pageNumber, String id, boolean value)
	{
		for (Page page : pageList)
		{
			if (page.getPageNumber() == pageNumber)
			{
				page.setDisplayTitleImage(id,value);
			}
		}
	}
	
	/**
	 * Method setTitreImagePage >>> sets the title of a given picture inside a given page.
	 * @param pageNumber : int
	 * @param id : String
	 * @param title : String
	 */
	public void setTitreImagePage(int pageNumber, String id, String title)
	{
		for (Page page : pageList)
		{
			if (page.getPageNumber() == pageNumber)
			{
				page.setTitreImage(id,title);
			}
		}
	}
	
	/**
	 * Method setCadreImagePage >>> sets the border style of a given picture from a given page.
	 * @param pageNumber : int
	 * @param id : String
	 * @param cadre : String
	 */
	public void setCadreImagePage(int pageNumber, String id, String cadre)
	{
		for (Page page : pageList)
		{
			if (page.getPageNumber() == pageNumber)
			{
				page.setCadreImage(id, cadre);
			}
		}
	}
	
	/**
	 * Method setCadreColorImagePage >>> sets the border color of a given picture from a given page.
	 * @param pageNumber : int
	 * @param id : String
	 * @param color : Stirng
	 */
	public void setCadreColorImagePage(int pageNumber, String id, String color)
	{
		for (Page page : pageList)
		{
			if (page.getPageNumber() == pageNumber)
			{
				page.setCadreColorImage(id, color);
			}
		}
	}
	
	/**
	 * Method getWidthImagePage >>> returns a given pictures's width from a given page.
	 * @param pageNumber : int
	 * @param id : String
	 * @return width : int
	 */
	public int getWidthImagePage(int pageNumber, String id)
	{
		for (Page page : pageList)
		{
			if (page.getPageNumber() == pageNumber)
			{
				return page.getWidthImage(id);
			}
		}
		return 0;
	}
	
	/**
	 * Method getCadreImagePage >>> gets the border style of a given image from a given page.
	 * @param pageNumber : int
	 * @param id : String
	 * @return cadre : String
	 */
	public String getCadreImagePage(int pageNumber, String id)
	{
		for (Page page : pageList)
		{
			if (page.getPageNumber() == pageNumber)
			{
				return page.getCadreImage(id);
			}
		}
		return "";
	}
	
	/**
	 * Method getCadreColorImagePage >>> gets the border color of a given image from a given page.
	 * @param pageNumber : int
	 * @param id : String
	 * @return color : String
	 * @param pageNumber
	 * @param id
	 * @return
	 */
	public String getCadreColorImagePage (int pageNumber, String id) {
		for (Page page : pageList)
		{
			if (page.getPageNumber() == pageNumber)
			{
				return page.getCadreColorImage(id);
			}
		}
		return "";
	}
	
	/**
	 * Method getWidthImagePage >>> returns a given pictures's height from a given page.
	 * @param pageNumber : int
	 * @param id : String
	 * @return height : int
	 */
	public int getHeightImagePage(int pageNumber, String id)
	{
		for (Page page : pageList)
		{
			if (page.getPageNumber() == pageNumber)
			{
				return page.getHeightImage(id);
			}
		}
		return 0;
	}
	
	/**
	 * Method getTitreImagePage >>> gets the title of a given picture from a given page.
	 * @param pageNumber : int
	 * @param id : String
	 * @return titreImage : String
	 */
	public String getTitreImagePage(int pageNumber, String id)
	{
		for (Page page : pageList)
		{
			if (page.getPageNumber() == pageNumber)
			{
				return page.getTitreImage(id);
			}
		}
		return null;
	}
	
	/**
	 * Method setPageList >>> sets the pageList attribute.
	 * @param pageList : ObservableList<Page>
	 */
	public void setPageList (ObservableList<Page> pageList) {
		this.pageList = pageList;
	}
	
	/**
	 * Method getPageList >>> gets the pageList attribute.
	 * @return pageList : ObservableList<Page>
	 */
	public ObservableList<Page> getPageList () {
		return this.pageList;
	}
	
	/**
	 * Method getPictureList >>> returns the images attributes
	 * @return images : ObservableList<Picture>
	 */
	public ObservableList<Picture> getPictureList() {
		return this.images;
	}

	/**
	 * Method setDisplayCadreImagePage : sets wether the border of a given picture from a
	 * given page should be displayed.
	 * @param pageNumber : int
	 * @param id : String
	 * @param value : boolean
	 */
	public void setDisplayCadreImagePage(int pageNumber, String id, boolean value) 
	{
		for (Page page : pageList)
		{
			if (page.getPageNumber() == pageNumber)
			{
				page.setDisplayCadreImage(id,value);
			}
		}
		
	}

	/**
	 * Method isDisplayTitleImagePage : gets wether the title of a given picture from a
	 * given page should be displayed.
	 * @param pageNumber : int
	 * @param id : String
	 * @param value : boolean
	 */
	public boolean isDisplayTitleImagePage(int pageNumber, String id) 
	{
		for (Page page : pageList)
		{
			if (page.getPageNumber() == pageNumber)
			{
				return page.isDisplayTitleImage(id);
			}
		}
		return false;
	}

	/**
	 * Method isDisplayCadreImagePage : gets wether the border of a given picture from a
	 * given page should be displayed.
	 * @param pageNumber : int
	 * @param id : String
	 * @param value : boolean
	 */
	public boolean isDisplayCadreImagePage(int pageNumber, String id) 
	{
		for (Page page : pageList)
		{
			if (page.getPageNumber() == pageNumber)
			{
				return page.isDisplayCadreImage(id);
			}
		}
		return false;
	}
	
}