package controller;

import model.Picture;

public class PictureController {

	private Picture pictureModel;
	
	/**
	 * Constructor PictureController >>> sets the attribute pictureModel
	 * @param pictureModel
	 */
	public PictureController (Picture pictureModel) {
		this.pictureModel = pictureModel;
	}
	
}
