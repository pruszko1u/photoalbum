package controller;
import model.Page;

public class PageController {

	private Page pageModel;
	
	/**
	 * Constructor PageController >>> sets the attribute pageModel
	 * @param pageModel
	 */
	public PageController (Page pageModel) {
		this.pageModel = pageModel;
	}
	
}
