package controller;

import java.util.ArrayList;

import application.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class OptionController 
{
	private AlbumController aContr;
	private int pageNbr;
	private String idPage;
	
	@FXML
	private TextField textfieldTitle;
	@FXML
	private CheckBox checkboxDisplayTitle;
	@FXML
	private TextField textfieldHeight;
	@FXML
	private TextField textfieldWidth;
	@FXML
	private ChoiceBox<String> choiceboxCadre;
	@FXML
	private ChoiceBox<String> choiceboxColor;
	@FXML
	private CheckBox checkboxDisplayCadre;
	@FXML
	private Button okButton;
	@FXML
	private Button cancelButton;
	
	
	/**
	 * Constructor NavigationController
	 * @param albumController 
	 * @param id 
	 * @param pageNumber 
	 */
	public OptionController(AlbumController albumController, int pageNumber, String id) 
	{
		aContr = albumController;
		pageNbr = pageNumber;
		idPage = id;
	}
	
	/**
	 * Method initialize >>> first method called on launch
	 * Intilializes the whole pages according to the picture we want to customize.
	 */
	@FXML
	private void initialize() 
	{
		this.initChoiceCadre();
		this.initChoiceColor();
		
		textfieldTitle.setText(aContr.main.album.getTitreImagePage(pageNbr, idPage));
		checkboxDisplayTitle.setSelected(aContr.main.album.isDisplayTitleImagePage(pageNbr, idPage));
		
		textfieldWidth.setText(Integer.toString(aContr.main.album.getWidthImagePage(pageNbr, idPage)));
		textfieldHeight.setText(Integer.toString(aContr.main.album.getHeightImagePage(pageNbr, idPage)));
		
		checkboxDisplayCadre.setSelected(aContr.main.album.isDisplayCadreImagePage(pageNbr, idPage));
	}
	
	/**
	 * Method onOK >>> method called when the "OK" button is clicked. Check all changes from the 
	 * option window and apply them to the picture. Then, closes the option window.
	 */
	@FXML
	protected void onOK()
	{	
		aContr.main.album.setTailleImagePage(pageNbr, idPage, Integer.parseInt(textfieldWidth.getText()),Integer.parseInt(textfieldHeight.getText()));
		aContr.main.album.setTitreImagePage(pageNbr, idPage, textfieldTitle.getText());
		aContr.main.album.setDisplayTitleImagePage(pageNbr, idPage, checkboxDisplayTitle.isSelected());
		aContr.main.album.setDisplayCadreImagePage(pageNbr, idPage, checkboxDisplayCadre.isSelected());
		
		String choiceCadre = "";
		if (choiceboxCadre.getSelectionModel().getSelectedItem() != null) 
			choiceCadre = choiceboxCadre.getSelectionModel().getSelectedItem().toString();
		
		
		aContr.main.album.setCadreImagePage(pageNbr, idPage, choiceCadre);
		
		String choiceCadreColor= "";
		if (choiceboxColor.getSelectionModel().getSelectedItem() != null) 
			choiceCadreColor = choiceboxColor.getSelectionModel().getSelectedItem().toString();
		
		
		aContr.main.album.setCadreColorImagePage(pageNbr, idPage, choiceCadreColor);
		
		aContr.updatePage();
		
		Stage stage = (Stage) okButton.getScene().getWindow();
	    stage.close();
	}
	
	/**
	 * Method onCancel >>> called when the "cancel" button is clicked from the option window.
	 * Closes the option window.
	 */
	@FXML
	protected void onCancel()
	{
		Stage stage = (Stage) cancelButton.getScene().getWindow();
	    stage.close();
	}
	
	/**
	 * Method initChoiceCadre >>> get the current border of the picture and
	 * set it as default in the matching ChoiceBox.
	 */
	protected void initChoiceCadre() {
		ArrayList<String> cadres = new ArrayList<String>();
		cadres.add("aucun");
		cadres.add("solid");
		cadres.add("doted");
		cadres.add("dashed");
		ObservableList<String> list = FXCollections.observableArrayList(cadres);
		choiceboxCadre.setItems(list);
		
		int selectedCadre = 0;
		
		switch(aContr.main.album.getCadreImagePage(pageNbr, idPage)) {
		case "solid": selectedCadre = 1;
		break;
		case "doted": selectedCadre = 2;
		break;
		case "dashed": selectedCadre = 3;
		break;
		default: selectedCadre = 0;
		break;
		}
		
		choiceboxCadre.getSelectionModel().select(selectedCadre);
	}
	
	/**
	 * Method initChoiceColor >>> get the current color of the picture's border and
	 * set it as default in the matching ChoiceBox.
	 */
	protected void initChoiceColor() {
		ArrayList<String> colors = new ArrayList<String>();
		colors.add("black");
		colors.add("red");
		colors.add("blue");
		colors.add("green");
		ObservableList<String> list = FXCollections.observableArrayList(colors);
		choiceboxColor.setItems(list);
		
		int selectedColor = 0;
		
		switch(aContr.main.album.getCadreColorImagePage(pageNbr, idPage)) {
		case "red": selectedColor = 1;
		break;
		case "blue": selectedColor = 2;
		break;
		case "green": selectedColor = 3;
		break;
		default: selectedColor = 0;
		break;
		}
		
		choiceboxColor.getSelectionModel().select(selectedColor);
	}
}
