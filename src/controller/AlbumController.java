package controller;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import application.Main;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Pair;
import model.Album;
import model.Page;
import model.Picture;

public class AlbumController {

	public Main main;
	private int pageNumber;
	private ImageView imageSource;
	
	private int widthOption;
	private int heightOption;

	@FXML
	private Button ajoutImage;
	@FXML
	private HBox listeImages;
	@FXML
	private AnchorPane anchorPane;
	@FXML
	private Pane pagePane;
	@FXML
	private Button buttonArrowLeft;
	@FXML
	private Button buttonArrowRight;

	/**
	 * Constructor AlbumController
	 */
	public AlbumController() {}

	/**
	 * Method setMain >>> used to set the main attribute, set the pageNumber and set the dragAndDrop on
	 * the displayed page (which both need to use the main). 
	 * @param main : Main
	 */
	public void setMain(Main main, int pageNumber) {
		this.main = main;
		AlbumController tempAlbumCtrl = this;

		this.pageNumber = this.main.album.pageExist(pageNumber) ? pageNumber : -1;

		this.updateAlbum();
		
		pagePane.setOnMouseReleased(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) 
			{
				if(event.getButton() == MouseButton.SECONDARY)
				{
					onOptionImg(tempAlbumCtrl.pageNumber, ((Node) event.getTarget()).getId());
				}
				
				tempAlbumCtrl.updatePage();
				event.consume();
			}
		});

		pagePane.setOnDragOver(new EventHandler<DragEvent>() {
			public void handle(DragEvent event) {
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
				event.consume();
			}
		});

		pagePane.setOnDragDropped(new EventHandler<DragEvent>() {
			public void handle(DragEvent event) {
				/* if there is a string data on dragboard, read it and use it */
				Dragboard db = event.getDragboard();
				boolean success = false;
				if (db.hasString()) {
					
					String source = db.getString().split("\\|")[0]; 
					String id = db.getString().split("\\|")[1]; 
					
					if (source.equals("liste")) {
						main.album.addImageAlbum(tempAlbumCtrl.pageNumber, id, event.getX(), event.getY());
						success = true;
					} else if (source.equals("page")) {
						main.album.setPositionImagePage(tempAlbumCtrl.pageNumber, id, event.getX(), event.getY());
						success = true;
					}
				
				}
				
				System.out.println(db.getString());

				tempAlbumCtrl.updatePage();

				event.setDropCompleted(success);

				event.consume();
			}
		});
	}
	
	/**
	 * Method getWidthOption >>> return the width attribute matching the option window's width
	 * @return widthOption : int
	 */
	public int getWidthOption() {
		return widthOption;
	}
	
	/**
	 * Method setWidthOption >>> set the width attribute matching the option window's width
	 * @param widthOption : void
	 */
	public void setWidthOption(int widthOption) {
		this.widthOption = widthOption;
	}

	/**
	 * Method getHeightOption >>> return the height attribute matching the option window's height
	 * @return
	 */
	public int getHeightOption() {
		return heightOption;
	}
	
	/**
	 * Method setHeightOption >>> set the height attribute matching the option window's height
	 * @param heightOption
	 */
	public void setHeightOption(int heightOption) {
		this.heightOption = heightOption;
	}

	/**
	 * Method goToNavigation >>> call the main's method "goToNavigation", but needed here because it
	 * is called in the FXML.
	 */
	public void goToNavigation() {
		this.main.goToNavigation(this.pageNumber);
	}
	
	/**
	 * Method goToHome >>> call the main's method "goToHome", but needed here because it
	 * is called in the FXML.
	 */
	public void goToHome() {
		this.main.goToHome();
	}
	
	/**
	 * Method onOptionImg >>> call the main's method "goToOption", but needed here because it
	 * is called in the FXML.
	 * This method is called when the user performs right click on a picture.
	 */
	public void onOptionImg(int pageNumber, String id)
	{
		try {
			this.main.goToOption(this,pageNumber,id);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Method onAddImage >>> called from album.fxml, adds a new picture.
	 */
	@FXML
	public void onAddImage() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		List<File> list = fileChooser.showOpenMultipleDialog(this.main.primaryStage);
		if (list != null) {
			for (File file : list) {
				this.main.album.addImage(file);
			}
		}

		this.updateAlbum();
	}

	/**
	 * Method onAddPage >>> called from album.fxml, adds a new page.
	 */
	@FXML
	public void onAddPage() {
		this.pageNumber = this.main.album.addPage();
		this.updatePage();
	}

	/**
	 * Method updateAlbum >>> single method which call both updatePage and updateListImage.
	 */
	public void updateAlbum() {
		this.updatePage();
		this.updateListeImage();
	}

	/**
	 * Method updateListeImage >>> Update the image list at the bottom of the screen.
	 */
	public void updateListeImage() {
		// retrieve the pictures for them to be displayed.
		ObservableList<Picture> pictures = this.main.album.getPictureList();
		ImageView[] imageView = new ImageView[pictures.size()];

		int i = 0;

		// each picture should be put into an ImageView, with the matching Id, width and height.
		for (Picture picture : this.main.album.getPictureList()) {
			Image img = picture.getImage();
			ImageView temp = new ImageView(img);
			temp.setId(picture.getId());
			temp.setFitHeight(100);
			//temp.setFitWidth(200);
			temp.setSmooth(true);
			temp.setPreserveRatio(true);

			
			// Add the onDragDetected event, in order to drag and drop the picture onto a page.
			temp.setOnDragDetected(new EventHandler<MouseEvent>() {
				public void handle(MouseEvent event) {
					// allow any transfer mode
					Dragboard db = temp.startDragAndDrop(TransferMode.ANY);

					// put a string (in this case, the picture's id) on dragboard
					ClipboardContent content = new ClipboardContent();
					content.putString("liste|" + temp.getId());
					db.setContent(content);

					event.consume();
				}
			});

			imageView[i] = temp;

			i++;
		}

		listeImages.getChildren().clear();
		listeImages.getChildren().addAll(imageView);
	}

	/**
	 * Method updatePage >>> Update the page that is currently displayed.
	 */
	public void updatePage() {

		// if pageNumber is different from -1, the matching page should be displayed.
		if (this.pageNumber != -1) {

			// pagePane is enabled + colored in white.
			this.pagePane.setDisable(false);
			this.pagePane.setStyle("-fx-background-color: white;");

			// the ajoutImage button is enable + colored in white.
			this.ajoutImage.setDisable(false);
			this.pagePane.setStyle("-fx-background-color: white;");

			// check if navigation buttons should be displayed.
			this.shouldShowButtonArrows();

			// get all pictures from the matching page for them to be displayed.
			ObservableList<Picture> pictures = this.main.album.getPicturesFromPage(this.pageNumber);

			if (pictures != null) {
				BorderPane[] imageViewWrapper = new BorderPane [pictures.size()];
				Label[] titres = new Label[pictures.size()];

				int i = 0;

				for (Picture picture : pictures) 
				{
					Image img = picture.getImage();
					ImageView temp = new ImageView(img);
					BorderPane imageWrapper = new BorderPane(temp);
					
					temp.setId(picture.getId());
					temp.setX(picture.getX());
					temp.setY(picture.getY());
					temp.setFitWidth(picture.getWidth());
					temp.setFitHeight(picture.getHeight());
					
					imageWrapper.setId(picture.getId());
					imageWrapper.setLayoutX(picture.getX());
					imageWrapper.setLayoutY(picture.getY());
					imageWrapper.prefHeight(picture.getHeight());
					imageWrapper.prefWidth(picture.getWidth());
					
					// Add the onDragDetected event, in order to drag and drop the picture onto a page.
					temp.setOnDragDetected(new EventHandler<MouseEvent>() {
						public void handle(MouseEvent event) {
							// allow any transfer mode
							Dragboard db = imageWrapper.startDragAndDrop(TransferMode.ANY);

							// put a string (in this case, the picture's id) on dragboard
							ClipboardContent content = new ClipboardContent();
							content.putString("page|" + imageWrapper.getId());
							db.setContent(content);

							event.consume();
						}
					});
					temp.setOnMousePressed(new EventHandler<MouseEvent>() {
						public void handle(MouseEvent event) 
						{
							if(event.getButton() == MouseButton.SECONDARY)
							{
								imageSource = (ImageView)event.getSource();
							}
						}
					});
					
					if (picture.getCadre() != "" && picture.getCadre() != null && picture.isDisplayCadre()) {
						
						String option = "";
						
						switch (picture.getCadre()) {
						case "solid": option = "solidBorder";
						break;
						case "doted": option = "dotedBorder";
						break;
						case "dashed": option = "dashedBorder";
						break;
						default: option = "";
						break;
						}
						
						String color = "";
						
						if (picture.getCadreColor() != "" && picture.getCadreColor() != null)
							color = picture.getCadreColor();
						
						imageWrapper.getStyleClass().add(option);
						imageWrapper.setStyle("-fx-border-color:" + color + ";");
					}
					imageViewWrapper[i] = imageWrapper;
					
					Label titre;
					if (picture.isDisplayTitle()) {
						titre = new Label(picture.getTitle());
						titre.setTranslateX(picture.getX());
						titre.setTranslateY(picture.getY() + picture.getHeight() + 10);
						titre.setPrefWidth(picture.getWidth());
						titre.getStyleClass().add("imageName");
					} else {
						titre = new Label();
					}

					titres[i] = titre;

					i++;
				}

				pagePane.getChildren().clear();
				pagePane.getChildren().addAll(imageViewWrapper);
				pagePane.getChildren().addAll(titres);
				
				
				WritableImage image = pagePane.snapshot(new SnapshotParameters(), null);
				 this.main.album.updatePageSnapshot(image, this.pageNumber);
			}
		} else {
			
			// If pageNumber is at -1, then pagePane and ajoutImage should be disabled and greyed.
			this.pagePane.setDisable(true);
			this.pagePane.setStyle("-fx-background-color: lightgrey;");

			this.ajoutImage.setDisable(true);
			this.pagePane.setStyle("-fx-background-color: lightgrey;");

			// Navigation buttons should be hidden.
			this.buttonArrowRight.setVisible(false);
			this.buttonArrowLeft.setVisible(false);
		}

	}

	/**
	 * Method souldShowButtonArrows >>> show or hide the navigation buttons.
	 * - If a previous page does exist, buttonArrowLeft is set to showed. If not, it's hidden.
	 * - If a next page does exist, the same goes for buttonArrowRight. 
	 */
	public void shouldShowButtonArrows() {
		if (this.pageNumber > 0) {
			this.buttonArrowLeft.setVisible(true);
		} else {
			this.buttonArrowLeft.setVisible(false);
		}

		this.main.album.getPageList();

		if (this.pageNumber > -1 && this.pageNumber < (this.main.album.getPageList().size() - 1)) {
			this.buttonArrowRight.setVisible(true);
		} else {
			this.buttonArrowRight.setVisible(false);
		}

	}

	/**
	 * Method goToPrevious >>> open previous page
	 */
	public void goToPrevious() {
		this.pageNumber--;
		this.updatePage();
	}

	/**
	 * Method goToNext >>> open next page
	 */
	public void goToNext() {
		this.pageNumber++;
		this.updatePage();
	}
	
}
