package controller;

import application.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import model.Page;

public class NavigationController {

	private Main main;
	private int displayedPageNumber;
	
	@FXML
	private Pane imageGauchePlus;
	@FXML
	private Pane imageGauche;
	@FXML
	private Pane imageCentre;
	@FXML
	private Pane imageDroitePlus;
	@FXML
	private Pane imageDroite;
	
	@FXML
	private Pane panelNavigation;
	
	@FXML 
	private Label noPageLabel;
	
	@FXML
	private Button buttonArrowLeft;
	@FXML
	private Button buttonArrowRight;
	
	/**
	 * Constructor NavigationController
	 */
	public NavigationController() {}
	
	/**
	 * Method setMain >>> sets the attribute Main and calls updateImagesRecentes
	 * @param main
	 */
	public void setMain(Main main, int pageNumber) {
		this.main = main;
		
		this.displayedPageNumber = this.main.album.pageExist(pageNumber) ? pageNumber : -1;
		
		this.updateNavigation();
	}
	
	/**
	 * Method updateNavigation >>> update the whole navigation pane.
	 */
	public void updateNavigation() {
		
		if (this.displayedPageNumber != -1) {

			this.noPageLabel.setVisible(false);
			this.panelNavigation.setStyle("-fx-background-color: white;");
			
			ObservableList<Page> pagesAlbum = this.main.album.getPageList();
			ObservableList<Page> pagesToDisplay = FXCollections.observableArrayList();
			
			if (pagesAlbum.size() > 0) {
				int i = 0;
				for (Page page : pagesAlbum) {
					if (page.getPageNumber() == this.displayedPageNumber) {

						// are there pages before the one we should display ?
						pagesToDisplay.add(i > 1 ? pagesAlbum.get(i - 2) : null);
						pagesToDisplay.add(i > 0 ? pagesAlbum.get(i - 1) : null);
						
						pagesToDisplay.add(page);
						
						// are there pages after the one we should display ?
						pagesToDisplay.add(i < pagesAlbum.size() - 1 ? pagesAlbum.get(i + 1) : null);
						pagesToDisplay.add(i < pagesAlbum.size() - 2 ? pagesAlbum.get(i + 2) : null);
					}
					
					i++;
				}
			}
			
			ImageView[] imageView = new ImageView[pagesToDisplay.size()];
			
			int j = 0;
			
			for (Page page : pagesToDisplay) {
				
				if (page != null) {
					
					WritableImage snapshot = page.getPageSnapshot();
					imageView[j] = new ImageView(snapshot);
					imageView[j].setId(String.valueOf(page.getPageNumber()));
		            imageView[j].setEffect(new DropShadow(10, Color.BLACK));
					
		            imageView[j].addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

		                @Override
		                public void handle(MouseEvent event) {
		                	main.goToAlbum(page.getPageNumber());
		                }
		           });
		            
					switch (j) {
					case 0 :
						imageView[j].setFitWidth(this.imageGauchePlus.getWidth());
						imageView[j].setFitHeight(this.imageGauchePlus.getHeight());
						this.imageGauchePlus.setVisible(true);
						imageGauchePlus.getChildren().clear();
						imageGauchePlus.getChildren().add(imageView[j]);
						break;
					case 1 :
						imageView[j].setFitWidth(this.imageGauche.getWidth());
						imageView[j].setFitHeight(this.imageGauche.getHeight());
						this.imageGauche.setVisible(true);
						imageGauche.getChildren().clear();
						imageGauche.getChildren().add(imageView[j]);
						break;
					case 2:
						imageView[j].setFitWidth(this.imageCentre.getWidth());
						imageView[j].setFitHeight(this.imageCentre.getHeight());
						this.imageCentre.setVisible(true);
						imageCentre.getChildren().clear();
						imageCentre.getChildren().add(imageView[j]);
						break;
					case 3:
						imageView[j].setFitWidth(this.imageDroite.getWidth());
						imageView[j].setFitHeight(this.imageDroite.getHeight());
						this.imageDroite.setVisible(true);
						imageDroite.getChildren().clear();
						imageDroite.getChildren().add(imageView[j]);
						break;
					case 4:
						imageView[j].setFitWidth(this.imageDroitePlus.getWidth());
						imageView[j].setFitHeight(this.imageDroitePlus.getHeight());
						this.imageDroitePlus.setVisible(true);
						imageDroitePlus.getChildren().clear();
						imageDroitePlus.getChildren().add(imageView[j]);
						break;
					}
					
				} else {
					switch (j) {
					case 0 :
						this.imageGauchePlus.setVisible(false);
						break;
					case 1 :
						this.imageGauche.setVisible(false);
						break;
					case 2:
						this.imageCentre.setVisible(false);
						break;
					case 3:
						this.imageDroite.setVisible(false);
						break;
					case 4:
						this.imageDroitePlus.setVisible(false);
						break;
					}
					
						
				}

	            j++;
	            
			}
			
		} else {
			this.imageGauchePlus.setVisible(false);
			this.imageGauche.setVisible(false);
			this.imageCentre.setVisible(false);
			this.imageDroite.setVisible(false);
			this.imageDroitePlus.setVisible(false);

			this.noPageLabel.setVisible(true);
			this.panelNavigation.setStyle("-fx-background-color: lightgrey;");
		}
		
		this.shouldShowButtonArrows();
	}
	
	/**
	 * Method goToAlbum >>> call the main's method "goToAlbum", but needed here because it
	 * is called in the FXML.
	 */
	public void goToAlbum() {
		this.main.goToAlbum(this.displayedPageNumber);
	}
	
	/**
	 * Method goToHome >>> call the main's method "goToHome", but needed here because it
	 * is called in the FXML.
	 */
	public void goToHome() {
		this.main.goToHome();
	}
	
	/**
	 * Method souldShowButtonArrows >>> show or hide the navigation buttons.
	 * - If a previous page does exist, buttonArrowLeft is set to showed. If not, it's hidden.
	 * - If a next page does exist, the same goes for buttonArrowRight. 
	 */
	public void shouldShowButtonArrows() {
		if (this.displayedPageNumber > 0) {
			this.buttonArrowLeft.setVisible(true);
		} else {
			this.buttonArrowLeft.setVisible(false);
		}

		this.main.album.getPageList();

		if (this.displayedPageNumber > -1 && this.displayedPageNumber < (this.main.album.getPageList().size() - 1)) {
			this.buttonArrowRight.setVisible(true);
		} else {
			this.buttonArrowRight.setVisible(false);
		}

	}

	/**
	 * Method goToPrevious >>> open previous page
	 */
	public void goToPrevious() {
		this.displayedPageNumber--;
		this.updateNavigation();
	}

	/**
	 * Method goToNext >>> open next page
	 */
	public void goToNext() {
		this.displayedPageNumber++;
		this.updateNavigation();
	}
	
	
}
