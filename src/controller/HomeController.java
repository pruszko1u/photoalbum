package controller;

import java.util.Collections;
import java.util.List;

import application.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import model.Page;
import model.Picture;

public class HomeController 
{
	@FXML
	private Button albumButton;
	
	@FXML
	private GridPane imagesRecentes;
	
	@FXML
	private GridPane pagesRecentes;
	
	@FXML 
	private Label noPageLabel;
	
	private Main main;
	
	/**
	 * Constructor HomeController
	 */
	public HomeController() {}
	
	/**
	 * Method setMain >>> sets the attribute Main and calls updateImagesRecentes
	 * @param main
	 */
	public void setMain(Main main) {
		this.main = main;
		this.updatePagesRecentes();
	}
	
	/**
	 * Method initialize
	 */
	@FXML
	private void initialize() {}

	
	/**
	 * Method goToAlbum >>> call the main's method "goToAlbum", but needed here because it
	 * is called in the FXML.
	 */
	@FXML 
	protected void goToAlbum() {
		this.main.goToAlbum(0);
    }
	
	/**
	 * Method goToNavigation >>> call the main's method "goToNavigation", but needed here because it
	 * is called in the FXML.
	 */
	public void goToNavigation() {
		this.main.goToNavigation(0);
	}

	
	/**
	 * Method updatePagesRecentes >>> gets the latest pages added for them to be displayed.
	 */
	public void updatePagesRecentes() {
		
		ObservableList<Page> pages = FXCollections.observableArrayList();
		ObservableList<Page> pagesAlbum = this.main.album.getPageList();
		
		
		if (pagesAlbum.size() > 0) {

			this.noPageLabel.setVisible(false);
			
			int limit = (pagesAlbum.size() > 8 ? pagesAlbum.size() - 9 : -1);
			
			for (int i = pagesAlbum.size() - 1 ; i > limit ; i--) {
				pages.add(pagesAlbum.get(i));
			}
			
		ImageView[] imageView = new ImageView[pages.size()];
		
		int i = 0;
		int h = 0;
		int v = 0;
		
		for (Page page : pages) {
			
			WritableImage snapshot = page.getPageSnapshot();
			imageView[i] = new ImageView(snapshot);
			imageView[i].setId(String.valueOf(page.getPageNumber()));
            imageView[i].setFitHeight(200);
            imageView[i].setFitWidth(200);
            imageView[i].setSmooth(true);
            imageView[i].setPreserveRatio(true);
            imageView[i].setEffect(new DropShadow(10, Color.BLACK));
            
            imageView[i].addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {
                	main.goToAlbum(page.getPageNumber());
                }
           });

            pagesRecentes.add(imageView[i], v, h);
    		
        		if (v >= 3) {
        			v = 0;
        			h++;
        		} else {
        			v++;
        		}
        		
                i++;

		}
		
		pagesRecentes.setHgap(20); //horizontal gap in pixels => that's what you are asking for
		pagesRecentes.setVgap(20); //vertical gap in pixels
		pagesRecentes.setPadding(new Insets(20, 20, 20, 20));

		} else {
			this.noPageLabel.setVisible(true);
		}
		
	}
}
