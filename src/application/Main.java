package application;

import java.io.IOException;

import controller.AlbumController;
import controller.HomeController;
import controller.NavigationController;
import controller.OptionController;
import controller.PageController;
import controller.PictureController;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Album;
import model.Home;
import model.Page;
import model.Picture;

public class Main extends Application {

	public Stage primaryStage;
	public Album album;
	
	/**
	 * Method start >>> first called method after launch
	 * @param primaryStage Stage 
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			this.album = new Album();
			this.primaryStage = primaryStage;
			primaryStage.setResizable(false);
			
			this.goToHome();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setScene(Scene scene) {
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	

	/**
	 * Method goToHome >>> displays the home screen.
	 */
	@FXML
	public void goToHome() {
		try {

			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/home/Home.fxml"));
			Pane root = fxmlLoader.load();

			Scene scene = new Scene(root, 1000, 700);
			scene.getStylesheets().add(getClass().getResource("/view/home/home.css").toExternalForm());
			this.setScene(scene);

			// Controller
			HomeController homeController;

			if (fxmlLoader.getController() instanceof HomeController) {
				homeController = (HomeController) fxmlLoader.getController();
				homeController.setMain(this);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method goToNavigation >>> displays the navigation screen.
	 * @param pageNumber : int
	 */
	@FXML
	public void goToNavigation(int pageNumber) {
		try {

			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/navigation/Navigation.fxml"));
			Pane root = fxmlLoader.load();

			Scene scene = new Scene(root, 1000, 700);
			scene.getStylesheets().add(getClass().getResource("/view/navigation/navigation.css").toExternalForm());
			this.setScene(scene);

			// Controller
			NavigationController navigationController;

			if (fxmlLoader.getController() instanceof NavigationController) {
				navigationController = (NavigationController) fxmlLoader.getController();
				navigationController.setMain(this, pageNumber);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Method goToAlbum >>> displays the album screen.
	 * @param pageNumber int 
	 */
	public void goToAlbum(int pageNumber) {
		try {
			
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/album/album.fxml"));
			Pane root = fxmlLoader.load();
			
			Scene scene = new Scene(root, 1000, 700);
			scene.getStylesheets().add(getClass().getResource("/view/album/album.css").toExternalForm());
			
			this.setScene(scene);			
			
			// Controller
			AlbumController albumController;
			
			if (fxmlLoader.getController() instanceof AlbumController) {
				albumController = (AlbumController) fxmlLoader.getController();
				
				if (albumController != null) {
					albumController.setMain(this, pageNumber);
				}
	        }
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Method main >>> main of PhotoAlbum
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * Method goToOption >>> displays the option screen (option popup on right click)
	 * @param albumController
	 * @param pageNumber
	 * @param id
	 * @throws IOException
	 */
	public void goToOption(AlbumController albumController, int pageNumber, String id) throws IOException 
	{	
		Stage primaryStage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/option/option.fxml"));
        loader.setController(new OptionController(albumController,pageNumber,id));
        Parent root = loader.load();

        Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("/view/option/option.css").toExternalForm());

        primaryStage.setScene(scene);
        primaryStage.initModality(Modality.APPLICATION_MODAL);
        primaryStage.initStyle(StageStyle.UTILITY);
        primaryStage.setResizable(false);
        primaryStage.showAndWait();
	}
}
